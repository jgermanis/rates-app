import { TestBed, async } from '@angular/core/testing';
import { HeaderComponent } from './header.component';
import { MockComponent } from 'ng-mocks';
import { MatToolbar } from '@angular/material/toolbar';

describe('HeaderComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        HeaderComponent,
        MockComponent(MatToolbar)
      ]
    }).compileComponents();
  }));

  it('should create the header', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const footer = fixture.componentInstance;
    expect(footer).toBeTruthy();
  });

  it('should match snapshot', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });
});
