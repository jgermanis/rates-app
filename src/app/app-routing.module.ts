import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'rates', pathMatch: 'full' },
  {
    path: 'rates',
    loadChildren: () => import('./rates/rates.module').then(m => m.RatesModule)
  },
  { path: '**', redirectTo: 'rates' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
