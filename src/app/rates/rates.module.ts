import { NgModule } from '@angular/core';
import { RatesRoutingModule } from './rates-routing.module';
import { RatesComponent } from './containers/rates/rates.component';
import { SingleRateComponent } from './containers/single-rate/single-rate.component';
import { StoreModule } from '@ngrx/store';
import * as fromRates from './store/reducers/rates.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RatesEffects } from './store/effects/rates.effects';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { BaseCurrencySelectorComponent } from './components/base-currency-selector/base-currency-selector.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { ChartsModule } from 'ng2-charts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
  imports: [
    CommonModule,
    RatesRoutingModule,
    StoreModule.forFeature(fromRates.ratesFeatureKey, fromRates.reducer),
    EffectsModule.forFeature([RatesEffects]),
    MatTableModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatIconModule,
    ChartsModule,
    MatProgressSpinnerModule
  ],
  exports: [],
  declarations: [
    RatesComponent,
    SingleRateComponent,
    BaseCurrencySelectorComponent,
    DatePickerComponent,
    ErrorComponent
  ]
})
export class RatesModule {}
