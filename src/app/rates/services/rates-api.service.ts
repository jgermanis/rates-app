import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { RatesApiResponse } from '../models/rates-api-response';
import { Currencies } from '../models/currencies';
import { HistoricRatesApiResponse } from '../models/historic-rates-api-response';

@Injectable({ providedIn: 'root' })
export class RatesApiService {
  private apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  getAllRates(
    baseCurrency: Currencies,
    date?: string
  ): Observable<RatesApiResponse> {
    return this.http.get<RatesApiResponse>(
      this.apiUrl + `/${date ? date : 'latest'}`,
      {
        params: {
          base: baseCurrency
        }
      }
    );
  }
  getRateHistory(
    baseCurrency: Currencies,
    startDate: string,
    endDate: string,
    currency: Currencies
  ): Observable<HistoricRatesApiResponse> {
    return this.http.get<HistoricRatesApiResponse>(this.apiUrl + '/history', {
      params: {
        start_at: startDate,
        end_at: endDate,
        base: baseCurrency,
        symbols: currency
      }
    });
  }
}
