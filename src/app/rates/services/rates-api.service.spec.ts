import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { RatesApiService } from './rates-api.service';
import { Currencies } from '../models/currencies';

describe('RatesApiService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let ratesApiService: RatesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RatesApiService]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    ratesApiService = TestBed.inject(RatesApiService);
  });

  describe('getAllRates', () => {
    it('should call correct endpoint when date provided', () => {
      ratesApiService.getAllRates(Currencies.GBP, '2019-01-01').subscribe();

      const req = httpTestingController.expectOne(
        'https://api.exchangeratesapi.io/2019-01-01?base=GBP'
      );

      expect(req.request.method).toEqual('GET');

      req.flush({});

      httpTestingController.verify();
    });

    it('should call correct endpoint when no date is provided', () => {
      ratesApiService.getAllRates(Currencies.GBP).subscribe();

      const req = httpTestingController.expectOne(
        'https://api.exchangeratesapi.io/latest?base=GBP'
      );

      expect(req.request.method).toEqual('GET');

      req.flush({});

      httpTestingController.verify();
    });
  });

  describe('getRateHistory', () => {
    it('should call correct endpoint', () => {
      ratesApiService
        .getRateHistory(
          Currencies.GBP,
          '2019-01-01',
          '2019-02-01',
          Currencies.CAD
        )
        .subscribe();

      const req = httpTestingController.expectOne(
        'https://api.exchangeratesapi.io/history?start_at=2019-01-01&end_at=2019-02-01&base=GBP&symbols=CAD'
      );

      expect(req.request.method).toEqual('GET');

      req.flush({});

      httpTestingController.verify();
    });
  });
});
