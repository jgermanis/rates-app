import { Currencies } from './models/currencies';
import { getCurrencyFlagClass } from './utils';

describe('utils', () => {
  describe('getCurrencyFlagClass', () => {
    it('should return correct class name', () => {
      const currencies =  [Currencies.EUR, Currencies.AUD, Currencies.GBP];
      const currencyClasses = currencies.map(currency => getCurrencyFlagClass(currency));
      const expectedClasses = ['currency-flag-eur', 'currency-flag-aud', 'currency-flag-gbp'];
      expect(currencyClasses).toEqual(expectedClasses);
    });
  });
});
