import { Currencies } from './models/currencies';

export const getCurrencyFlagClass = (currency: Currencies | string): string => {
  return `currency-flag-${currency}`.toLowerCase();
};
