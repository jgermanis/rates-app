export interface HistoricRates {
  dates: string[];
  rateValues: number[];
}
