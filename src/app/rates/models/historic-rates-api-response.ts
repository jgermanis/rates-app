import { Currencies } from './currencies';
import { Rates } from './rates';

export interface HistoricRatesApiResponse {
  base: Currencies;
  rates: { [key: string]: Rates };
  start_at: string;
  end_at: string;
}
