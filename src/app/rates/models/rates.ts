import { Currencies } from './currencies';

export type Rates = {
  [key in Currencies]: number;
};
