export enum LoadingStatus {
  loading,
  loaded,
  failed
}
