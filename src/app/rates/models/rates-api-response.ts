import { Currencies } from './currencies';
import { Rates } from './rates';

export interface RatesApiResponse {
  base: Currencies;
  rates: Rates;
  date: string;
}
