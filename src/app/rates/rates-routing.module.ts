import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RatesComponent } from './containers/rates/rates.component';
import { SingleRateComponent } from './containers/single-rate/single-rate.component';

const routes: Routes = [
  {
    path: ':id',
    component: SingleRateComponent
  },
  {
    path: '',
    component: RatesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RatesRoutingModule {}
