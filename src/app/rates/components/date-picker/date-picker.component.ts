import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerComponent implements OnInit {
  @Input() selectedDate: string;
  @Input() label: string;
  @Output() selectDate = new EventEmitter<string>();
  constructor() {}

  ngOnInit(): void {}

  onDateChange(date: MatDatepickerInputEvent<Date>) {
    this.selectDate.emit(date.value.toISOString().split('T')[0]);
  }
}
