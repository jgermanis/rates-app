import { TestBed, async } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import {
  MatDatepickerInputEvent,
  MatDatepickerModule
} from '@angular/material/datepicker';
import { DatePickerComponent } from './date-picker.component';
import * as MockDate from 'mockdate';
import { MatNativeDateModule } from '@angular/material/core';

describe('DatePickerComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatNativeDateModule, MatDatepickerModule],
      declarations: [
        DatePickerComponent,
        MockComponent(MatFormField),
        MockComponent(MatLabel)
      ]
    }).compileComponents();
  }));

  it('should create the component', () => {
    const fixture = TestBed.createComponent(DatePickerComponent);
    const datePickerComponent = fixture.componentInstance;
    expect(datePickerComponent).toBeTruthy();
  });

  it('should match snapshot', () => {
    const fixture = TestBed.createComponent(DatePickerComponent);
    const datePickerComponent = fixture.componentInstance;
    datePickerComponent.label = 'Test Label';
    datePickerComponent.selectedDate = '2019-02-02';
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });

  describe('onDateChange', () => {
    it('should emit new date', () => {
      MockDate.set(1584824399699);
      const fixture = TestBed.createComponent(DatePickerComponent);
      fixture.detectChanges();
      const datePickerComponent = fixture.componentInstance;
      spyOn(datePickerComponent.selectDate, 'emit');
      datePickerComponent.onDateChange({
        value: new Date()
      } as MatDatepickerInputEvent<Date>);
      expect(datePickerComponent.selectDate.emit).toHaveBeenCalledWith(
        '2020-03-21'
      );
      MockDate.reset();
    });
  });
});
