import { TestBed, async } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { BaseCurrencySelectorComponent } from './base-currency-selector.component';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import {
  MatSelect,
  MatSelectChange,
  MatSelectTrigger
} from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { provideMockStore } from '@ngrx/store/testing';
import {
  initialRatesState,
  RatesState
} from '../../store/reducers/rates.reducer';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { Currencies } from '../../models/currencies';

describe('BaseCurrencySelectorComponent', () => {
  let store: Store<RatesState>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        BaseCurrencySelectorComponent,
        MockComponent(MatFormField),
        MockComponent(MatLabel),
        MockComponent(MatSelect),
        MockComponent(MatSelectTrigger),
        MockComponent(MatOption)
      ],
      providers: [provideMockStore({ initialState: initialRatesState })]
    }).compileComponents();

    store = TestBed.inject(Store);
  }));

  it('should create the component', () => {
    const fixture = TestBed.createComponent(BaseCurrencySelectorComponent);
    const baseCurrencySelector = fixture.componentInstance;
    expect(baseCurrencySelector).toBeTruthy();
  });

  it('should match snapshot', () => {
    spyOn(store, 'select').and.returnValue(of(Currencies.GBP));
    const fixture = TestBed.createComponent(BaseCurrencySelectorComponent);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });

  describe('onSelectionChange', () => {
    it('should dispatch baseCurrency change action', () => {
      spyOn(store, 'dispatch');
      const fixture = TestBed.createComponent(BaseCurrencySelectorComponent);
      fixture.detectChanges();
      const baseCurrencySelector = fixture.componentInstance;
      baseCurrencySelector.onSelectionChange({
        value: Currencies.DKK
      } as MatSelectChange);
      expect(store.dispatch).toHaveBeenCalledWith({
        currency: 'DKK',
        type: '[Rates] Change Base Currency'
      });
    });
  });
});
