import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Currencies } from '../../models/currencies';
import { getCurrencyFlagClass } from '../../utils';
import { RatesState } from '../../store/reducers/rates.reducer';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getBaseCurrency } from '../../store/selectors/rates.selectors';
import { changeBaseCurrency } from '../../store/actions/rates.actions';
import { MatSelectChange } from '@angular/material/select';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-base-currency-selector',
  templateUrl: './base-currency-selector.component.html',
  styleUrls: ['./base-currency-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BaseCurrencySelectorComponent implements OnInit {
  baseCurrency$: Observable<Currencies>;
  baseCurrency: Currencies;
  currencies: string[] = Object.keys(Currencies);
  getCurrencyFlagClass = getCurrencyFlagClass;

  constructor(private store: Store<RatesState>) {}

  ngOnInit(): void {
    this.baseCurrency$ = this.store
      .select(getBaseCurrency)
      .pipe(tap(currency => (this.baseCurrency = currency)));
  }

  onSelectionChange(ev: MatSelectChange) {
    this.store.dispatch(changeBaseCurrency({ currency: ev.value }));
  }
}
