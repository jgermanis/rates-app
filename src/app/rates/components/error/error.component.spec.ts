import { TestBed, async } from '@angular/core/testing';
import { ErrorComponent } from './error.component';

describe('ErrorComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [ErrorComponent]
    }).compileComponents();
  }));

  it('should create the error', () => {
    const fixture = TestBed.createComponent(ErrorComponent);
    const errorComponent = fixture.componentInstance;
    expect(errorComponent).toBeTruthy();
  });

  it('should match snapshot', () => {
    const fixture = TestBed.createComponent(ErrorComponent);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });

  describe('reloadPage', () => {
    it('should call window.location.reload', () => {
      delete window.location;
      window.location = { reload: jest.fn() } as any;
      const fixture = TestBed.createComponent(ErrorComponent);
      const errorComponent = fixture.componentInstance;
      errorComponent.reloadPage();
      expect(window.location.reload).toHaveBeenCalled();
    });
  });
});
