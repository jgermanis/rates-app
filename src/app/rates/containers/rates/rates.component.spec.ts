import { async, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { RatesComponent } from './rates.component';
import { MatCard, MatCardContent, MatCardTitle } from '@angular/material/card';
import { BaseCurrencySelectorComponent } from '../../components/base-currency-selector/base-currency-selector.component';
import { MatSpinner } from '@angular/material/progress-spinner';
import { DatePickerComponent } from '../../components/date-picker/date-picker.component';
import { ErrorComponent } from '../../components/error/error.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {
  initialRatesState,
  RatesState
} from '../../store/reducers/rates.reducer';
import { MemoizedSelector } from '@ngrx/store';
import * as RatesSelectors from '../../store/selectors/rates.selectors';
import { LoadingStatus } from '../../models/loading-status';
import { Currencies } from '../../models/currencies';
import { Rates } from '../../models/rates';
import { By } from '@angular/platform-browser';

describe('RatesComponent', () => {
  let mockStore: MockStore;
  let mockRatesLoadingStatusSelector: MemoizedSelector<
    RatesState,
    LoadingStatus
  >;
  let mockDateSelector: MemoizedSelector<RatesState, string>;
  let mockBaseCurrencySelector: MemoizedSelector<RatesState, Currencies>;
  let mockRatesKeySelector: MemoizedSelector<RatesState, string>;
  let mockRatesSelector: MemoizedSelector<RatesState, Rates>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        RatesComponent,
        MockComponent(MatCard),
        MockComponent(MatSpinner),
        MockComponent(BaseCurrencySelectorComponent),
        MockComponent(DatePickerComponent),
        MockComponent(ErrorComponent),
        MockComponent(MatCardContent),
        MockComponent(MatCardTitle)
      ],
      providers: [provideMockStore({ initialState: initialRatesState })]
    }).compileComponents();

    mockStore = TestBed.inject(MockStore);
    mockBaseCurrencySelector = mockStore.overrideSelector(
      RatesSelectors.getBaseCurrency,
      Currencies.GBP
    );
    mockDateSelector = mockStore.overrideSelector(
      RatesSelectors.getDate,
      '2019-01-01'
    );
    mockRatesKeySelector = mockStore.overrideSelector(
      RatesSelectors.getRatesKey,
      'GBP_2019-01-01'
    );
    mockRatesSelector = mockStore.overrideSelector(
      RatesSelectors.getRates,
      null
    );
    mockRatesLoadingStatusSelector = mockStore.overrideSelector(
      RatesSelectors.getRatesLoadingStatus,
      LoadingStatus.loading
    );
  }));

  it('should create the component', () => {
    const fixture = TestBed.createComponent(RatesComponent);
    const ratesComponent = fixture.componentInstance;
    expect(ratesComponent).toBeTruthy();
  });

  it('should match snapshot when rates loadingStatus is loading', () => {
    const fixture = TestBed.createComponent(RatesComponent);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#rates-container'))).toMatchSnapshot();
  });

  it('should match snapshot when rates loadingStatus is failure', () => {
    mockRatesLoadingStatusSelector.setResult(LoadingStatus.failed);
    mockStore.refreshState();
    const fixture = TestBed.createComponent(RatesComponent);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#rates-container'))).toMatchSnapshot();
  });

  it('should match snapshot when rates loadingStatus is loaded', () => {
    mockRatesLoadingStatusSelector.setResult(LoadingStatus.loaded);
    mockStore.refreshState();
    const fixture = TestBed.createComponent(RatesComponent);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#rates-container'))).toMatchSnapshot();
  });

  it('should dispatch loadRates action once the key differs from store ', () => {
    spyOn(mockStore, 'dispatch');

    const fixture = TestBed.createComponent(RatesComponent);
    fixture.detectChanges();

    expect(mockStore.dispatch).not.toHaveBeenCalled();
    mockBaseCurrencySelector.setResult(Currencies.DKK);
    mockStore.refreshState();
    fixture.detectChanges();
    expect(mockStore.dispatch).toHaveBeenCalledWith({
      baseCurrency: 'DKK',
      date: '2019-01-01',
      type: '[Rates] Load Rates'
    });
  });

  describe('onSelectDate', () => {
    it('should dispatch  changeSelectedDate action', () => {
      const fixture = TestBed.createComponent(RatesComponent);
      fixture.detectChanges();
      const ratesComponent = fixture.componentInstance;
      spyOn(mockStore, 'dispatch');
      ratesComponent.onSelectDate('date');
      expect(mockStore.dispatch).toHaveBeenCalledWith({
        date: 'date',
        type: '[Rates] Change Selected Date'
      });
    });
  });
});
