import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { select, Store } from '@ngrx/store';
import { RatesState } from '../../store/reducers/rates.reducer';
import {
  changeSelectedDate,
  loadRates
} from '../../store/actions/rates.actions';
import { Currencies } from '../../models/currencies';
import { combineLatest, Observable, Subject } from 'rxjs';
import { Rates } from '../../models/rates';
import {
  getBaseCurrency,
  getDate,
  getRates,
  getRatesKey,
  getRatesLoadingStatus
} from '../../store/selectors/rates.selectors';
import { filter, map, takeUntil } from 'rxjs/operators';
import { getCurrencyFlagClass } from '../../utils';
import { LoadingStatus } from '../../models/loading-status';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RatesComponent implements OnInit, OnDestroy {
  selectedDate: string;
  ratesLoadingStatus$: Observable<LoadingStatus>;
  rates$: Observable<{ currency: Currencies; value: number }[]>;
  getCurrencyFlagClass = getCurrencyFlagClass;
  loadingStatus = LoadingStatus;

  private destroy$ = new Subject();

  constructor(private store: Store<RatesState>) {}

  ngOnInit(): void {
    this.ratesLoadingStatus$ = this.store.select(getRatesLoadingStatus);
    combineLatest(
      this.store.select(getDate),
      this.store.select(getBaseCurrency),
      this.store.select(getRatesKey)
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(([date, baseCurrency, keyFromStore]) => {
        this.selectedDate = date;
        const key = `${baseCurrency}_${date ? date : ''}`;
        if (key !== keyFromStore) {
          this.store.dispatch(loadRates({ baseCurrency, date }));
        }
      });

    this.rates$ = this.store.pipe(
      select(getRates),
      filter(rates => !!rates),
      map((rates: Rates) =>
        Object.keys(rates).map((key: Currencies) => ({
          currency: key,
          value: rates[key]
        }))
      )
    );
  }

  onSelectDate(date: string): void {
    this.store.dispatch(changeSelectedDate({ date }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
