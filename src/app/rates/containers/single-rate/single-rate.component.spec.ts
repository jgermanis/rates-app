import { async, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { MatCard } from '@angular/material/card';
import { BaseCurrencySelectorComponent } from '../../components/base-currency-selector/base-currency-selector.component';
import { MatSpinner } from '@angular/material/progress-spinner';
import { DatePickerComponent } from '../../components/date-picker/date-picker.component';
import { ErrorComponent } from '../../components/error/error.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {
  initialRatesState,
  RatesState
} from '../../store/reducers/rates.reducer';
import { MemoizedSelector } from '@ngrx/store';
import * as RatesSelectors from '../../store/selectors/rates.selectors';
import { LoadingStatus } from '../../models/loading-status';
import { Currencies } from '../../models/currencies';
import { SingleRateComponent } from './single-rate.component';
import { HistoricRates } from '../../models/historic-rates';
import { MatIcon } from '@angular/material/icon';
import { BaseChartDirective } from 'ng2-charts';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import * as MockDate from 'mockdate';
import { By } from '@angular/platform-browser';

describe('SingleRateComponent', () => {
  let mockStore: MockStore;
  let mockHistoricRatesLoadingStatusSelector: MemoizedSelector<
    RatesState,
    LoadingStatus
  >;
  let mockBaseCurrencySelector: MemoizedSelector<RatesState, Currencies>;
  let mockHistoricRatesSelector: MemoizedSelector<RatesState, HistoricRates>;
  const paramsSubject = new BehaviorSubject({ id: 'EUR' });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        SingleRateComponent,
        MockComponent(MatCard),
        MockComponent(MatSpinner),
        MockComponent(BaseCurrencySelectorComponent),
        MockComponent(DatePickerComponent),
        MockComponent(ErrorComponent),
        MockComponent(MatIcon),
        MockComponent(BaseChartDirective)
      ],
      providers: [
        provideMockStore({ initialState: initialRatesState }),
        {
          provide: ActivatedRoute,
          useValue: { params: paramsSubject.asObservable() }
        }
      ]
    }).compileComponents();

    mockStore = TestBed.inject(MockStore);
    mockBaseCurrencySelector = mockStore.overrideSelector(
      RatesSelectors.getBaseCurrency,
      Currencies.GBP
    );
    mockHistoricRatesSelector = mockStore.overrideSelector(
      RatesSelectors.getHistoricRates,
      null
    );
    mockHistoricRatesLoadingStatusSelector = mockStore.overrideSelector(
      RatesSelectors.getHistoricRatesLoadingStatus,
      LoadingStatus.loading
    );
    MockDate.set(1584824399699);
  }));

  afterEach(() => {
    MockDate.reset();
  });

  it('should create the component', () => {
    const fixture = TestBed.createComponent(SingleRateComponent);
    const singleRatesComponent = fixture.componentInstance;
    expect(singleRatesComponent).toBeTruthy();
  });

  it('should match snapshot when historic rates loadingStatus is loading', () => {
    const fixture = TestBed.createComponent(SingleRateComponent);
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#single-rates-container'))
        .nativeElement
    ).toMatchSnapshot();
  });

  it('should match snapshot when historic rates loadingStatus is failure', () => {
    mockHistoricRatesLoadingStatusSelector.setResult(LoadingStatus.failed);
    mockStore.refreshState();
    const fixture = TestBed.createComponent(SingleRateComponent);
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.css('#single-rates-container'))
        .nativeElement
    ).toMatchSnapshot();
  });

  it('should match snapshot when historic rates loadingStatus is loaded', () => {
    mockHistoricRatesLoadingStatusSelector.setResult(LoadingStatus.loaded);
    mockStore.refreshState();
    const fixture = TestBed.createComponent(SingleRateComponent);
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('#single-rates-container'))
        .nativeElement
    ).toMatchSnapshot();
  });

  it('should dispatch loadHistoicRates action on init', () => {
    spyOn(mockStore, 'dispatch');

    const fixture = TestBed.createComponent(SingleRateComponent);
    fixture.detectChanges();
    expect(mockStore.dispatch).toHaveBeenCalledWith({
      baseCurrency: 'GBP',
      currency: 'EUR',
      end_at: '2020-03-21',
      start_at: '2020-03-14',
      type: '[Rates] Load Historic Rates'
    });
  });

  describe('onSelectStartAt', () => {
    it('should dispatch  getHistoricRates action', () => {
      spyOn(mockStore, 'dispatch');
      const fixture = TestBed.createComponent(SingleRateComponent);
      fixture.detectChanges();
      const singleRatesComponent = fixture.componentInstance;
      let updatedStartAt: string;
      singleRatesComponent.startAt$.subscribe(
        startAt => (updatedStartAt = startAt)
      );
      singleRatesComponent.onSelectStartAt('2019-01-01');
      fixture.detectChanges();
      expect(updatedStartAt).toBe('2019-01-01');
      expect(mockStore.dispatch).toBeCalledTimes(2);
      expect(mockStore.dispatch).toHaveBeenLastCalledWith({
        baseCurrency: 'GBP',
        currency: 'EUR',
        end_at: '2020-03-21',
        start_at: '2019-01-01',
        type: '[Rates] Load Historic Rates'
      });
    });
  });

  describe('onSelectEndAt', () => {
    it("should dispatch  getHistoricRates action'", () => {
      spyOn(mockStore, 'dispatch');
      const fixture = TestBed.createComponent(SingleRateComponent);
      fixture.detectChanges();
      const singleRatesComponent = fixture.componentInstance;
      let updatedEndAt: string;
      singleRatesComponent.endAt$.subscribe(endAt => (updatedEndAt = endAt));
      singleRatesComponent.onSelectEndAt('2019-01-01');
      fixture.detectChanges();
      expect(updatedEndAt).toBe('2019-01-01');
      expect(mockStore.dispatch).toBeCalledTimes(2);
      expect(mockStore.dispatch).toHaveBeenLastCalledWith({
        baseCurrency: 'GBP',
        currency: 'EUR',
        end_at: '2019-01-01',
        start_at: '2020-03-14',
        type: '[Rates] Load Historic Rates'
      });
    });
  });
});
