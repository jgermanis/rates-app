import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { RatesState } from '../../store/reducers/rates.reducer';
import { loadHistoricRates } from '../../store/actions/rates.actions';
import { Currencies } from '../../models/currencies';
import {
  getBaseCurrency,
  getHistoricRates,
  getHistoricRatesLoadingStatus
} from '../../store/selectors/rates.selectors';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, takeUntil, tap } from 'rxjs/operators';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { LoadingStatus } from '../../models/loading-status';

@Component({
  selector: 'app-single-rate',
  templateUrl: './single-rate.component.html',
  styleUrls: ['./single-rate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleRateComponent implements OnInit, OnDestroy {
  historicRates$: Observable<{ data: ChartDataSets[]; labels: Label[] }>;
  startAt$ = new BehaviorSubject<string>(this.formatDate(new Date()));
  endAt$ = new BehaviorSubject<string>(this.formatDate(new Date()));
  lineChartOptions = {
    responsive: true
  };
  lineChartColors: Color[] = [
    {
      borderColor: '#c2185b',
      backgroundColor: 'rgba(194,24,91,0.28)'
    }
  ];
  loadingStatus = LoadingStatus;
  historicRatesLoadingStatus$: Observable<LoadingStatus>;

  private destroy$ = new Subject();
  private currency: string;

  constructor(
    private store: Store<RatesState>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.historicRatesLoadingStatus$ = this.store.select(
      getHistoricRatesLoadingStatus
    );
    const date = new Date();
    date.setDate(date.getDate() - 7);
    this.startAt$.next(this.formatDate(date));
    combineLatest(
      this.store.select(getBaseCurrency),
      this.startAt$,
      this.endAt$,
      this.route.params.pipe(tap(({ id }) => (this.currency = id)))
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(([baseCurrency, startAt, endAt, { id }]) => {
        if (!Object.keys(Currencies).some(currency => currency === id)) {
          return this.router.navigate(['/rates']);
        }
        this.store.dispatch(
          loadHistoricRates({
            baseCurrency,
            start_at: startAt,
            end_at: endAt,
            currency: id
          })
        );
      });
    this.historicRates$ = this.store.select(getHistoricRates).pipe(
      filter(rates => !!rates),
      map(historicRates => {
        return {
          data: [{ data: historicRates.rateValues, label: this.currency }],
          labels: historicRates.dates
        };
      })
    );
  }

  private formatDate(date: Date): string {
    return date.toISOString().split('T')[0];
  }

  onSelectStartAt(date: string): void {
    this.startAt$.next(date);
  }

  onSelectEndAt(date: string): void {
    this.endAt$.next(date);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
