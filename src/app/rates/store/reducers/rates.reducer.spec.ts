import { Currencies } from '../../models/currencies';
import * as actions from '../actions/rates.actions';
import { initialRatesState, reducer } from './rates.reducer';
import { LoadingStatus } from '../../models/loading-status';
import { Rates } from '../../models/rates';
import { RatesApiResponse } from '../../models/rates-api-response';

describe('RatesReducer', () => {
  describe('RatesActions.loadRates action', () => {
    it('should work', () => {
      const action = actions.loadRates({ baseCurrency: Currencies.USD });
      const result = reducer(initialRatesState, action);
      expect(result.ratesLoadingStatus).toEqual(LoadingStatus.loading);
    });
  });

  describe('RatesActions.loadRatesSuccess action', () => {
    it('should work', () => {
      const ratesApiResponse: RatesApiResponse = {
        date: '2019-01-01',
        base: Currencies.GBP,
        rates: {} as Rates
      };
      const action = actions.loadRatesSuccess({
        ratesApiResponse,
        ratesKey: 'key'
      });
      const result = reducer(initialRatesState, action);
      expect(result.ratesKey).toEqual('key');
      expect(result.ratesLoadingStatus).toEqual(LoadingStatus.loaded);
      expect(result.rates).toEqual(ratesApiResponse.rates);
    });
  });

  describe('RatesActions.loadRatesFailure action', () => {
    it('should work', () => {
      const action = actions.loadRatesFailure();
      const result = reducer(initialRatesState, action);
      expect(result.ratesLoadingStatus).toEqual(LoadingStatus.failed);
    });
  });

  describe('RatesActions.loadHistoricRates action', () => {
    it('should work', () => {
      const action = actions.loadHistoricRates({
        baseCurrency: Currencies.USD,
        start_at: '2019-01-01',
        end_at: '2019-01-09',
        currency: Currencies.EUR
      });
      const result = reducer(initialRatesState, action);
      expect(result.historicRatesLoadingStatus).toEqual(LoadingStatus.loading);
    });
  });

  describe('RatesActions.loadHistoricRatesSuccess action', () => {
    it('should work', () => {
      const action = actions.loadHistoricRatesSuccess({
        historicRates: {
          start_at: '2019-01-01',
          end_at: '2019-01-09',
          base: Currencies.GBP,
          rates: {
            '2019-01-01': { AUD: 1 } as Rates,
            '2019-01-05': { AUD: 2 } as Rates,
            '2019-01-02': { AUD: 3 } as Rates
          }
        },
        currency: Currencies.AUD
      });
      const result = reducer(initialRatesState, action);
      expect(result.historicRates).toEqual({
        dates: ['2019-01-01', '2019-01-02', '2019-01-05'],
        rateValues: [1, 3, 2]
      });
      expect(result.historicRatesLoadingStatus).toEqual(LoadingStatus.loaded);
    });
  });

  describe('RatesActions.loadHistoricRatesFailure action', () => {
    it('should work', () => {
      const action = actions.loadHistoricRatesFailure();
      const result = reducer(initialRatesState, action);
      expect(result.historicRatesLoadingStatus).toEqual(LoadingStatus.failed);
    });
  });

  describe('RatesActions.changeBaseCurrency action', () => {
    it('should work', () => {
      const action = actions.changeBaseCurrency({ currency: Currencies.USD });
      const result = reducer(initialRatesState, action);
      expect(result.baseCurrency).toEqual(Currencies.USD);
    });
  });

  describe('RatesActions.changeSelectedDate action', () => {
    it('should work', () => {
      const action = actions.changeSelectedDate({ date: 'date-string' });
      const result = reducer(initialRatesState, action);
      expect(result.date).toEqual('date-string');
    });
  });
});
