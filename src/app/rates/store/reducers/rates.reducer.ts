import { Action, createReducer, on } from '@ngrx/store';
import * as RatesActions from '../actions/rates.actions';
import { Currencies } from '../../models/currencies';
import { Rates } from '../../models/rates';
import { LoadingStatus } from '../../models/loading-status';
import { HistoricRates } from '../../models/historic-rates';

export const ratesFeatureKey = 'rates';

export interface RatesState {
  baseCurrency: Currencies;
  date: string | null;
  rates: Rates | null;
  ratesKey: string | null;
  ratesLoadingStatus: LoadingStatus;
  historicRates: HistoricRates | null;
  historicRatesLoadingStatus: LoadingStatus;
}

export const initialRatesState: RatesState = {
  baseCurrency: Currencies.EUR,
  date: null,
  rates: null,
  ratesKey: null,
  ratesLoadingStatus: LoadingStatus.loading,
  historicRates: null,
  historicRatesLoadingStatus: LoadingStatus.loading
};

const ratesReducer = createReducer(
  initialRatesState,
  on(RatesActions.loadRates, state => ({
    ...state,
    ratesLoadingStatus: LoadingStatus.loading
  })),
  on(
    RatesActions.loadRatesSuccess,
    (state, { ratesApiResponse, ratesKey }) => ({
      ...state,
      rates: ratesApiResponse.rates,
      ratesKey,
      ratesLoadingStatus: LoadingStatus.loaded
    })
  ),
  on(RatesActions.loadRatesFailure, state => ({
    ...state,
    ratesLoadingStatus: LoadingStatus.failed
  })),
  on(RatesActions.loadHistoricRates, state => ({
    ...state,
    historicRatesLoadingStatus: LoadingStatus.loading
  })),
  on(
    RatesActions.loadHistoricRatesSuccess,
    (state, { historicRates, currency }) => {
      const sortedDates = Object.keys(historicRates.rates).sort();
      const rateValues: number[] = sortedDates.map(
        date => historicRates.rates[date][currency]
      );
      return {
        ...state,
        historicRates: {
          dates: sortedDates,
          rateValues
        },
        historicRatesLoadingStatus: LoadingStatus.loaded
      };
    }
  ),
  on(RatesActions.loadHistoricRatesFailure, state => ({
    ...state,
    historicRatesLoadingStatus: LoadingStatus.failed
  })),
  on(RatesActions.changeBaseCurrency, (state, { currency }) => ({
    ...state,
    baseCurrency: currency
  })),
  on(RatesActions.changeSelectedDate, (state, { date }) => ({
    ...state,
    date
  }))
);

export function reducer(state: RatesState | undefined, action: Action) {
  return ratesReducer(state, action);
}
