import { RatesState } from '../reducers/rates.reducer';
import { Rates } from '../../models/rates';
import { LoadingStatus } from '../../models/loading-status';
import { Currencies } from '../../models/currencies';
import * as selectors from './rates.selectors';

describe('RateSelectors', () => {
  const state: RatesState = {
    baseCurrency: Currencies.GBP,
    date: '2012-12-12',
    rates: { [Currencies.EUR]: 1.12345 } as Rates,
    ratesKey: 'key',
    ratesLoadingStatus: LoadingStatus.loaded,
    historicRates: { dates: [], rateValues: [] },
    historicRatesLoadingStatus: LoadingStatus.failed
  };

  describe('getRates', () => {
    it('should select rates', () => {
      expect(selectors.getRates.projector(state)).toEqual(state.rates);
    });
  });

  describe('getHistoricRates', () => {
    it('should select HistoricRates', () => {
      expect(selectors.getHistoricRates.projector(state)).toEqual(
        state.historicRates
      );
    });
  });

  describe('getRatesLoadingStatus', () => {
    it('should select RatesLoadingStatus', () => {
      expect(selectors.getRatesLoadingStatus.projector(state)).toEqual(
        state.ratesLoadingStatus
      );
    });
  });

  describe('getHistoricRatesLoadingStatus', () => {
    it('should select HistoricRatesLoadingStatus', () => {
      expect(selectors.getHistoricRatesLoadingStatus.projector(state)).toEqual(
        state.historicRatesLoadingStatus
      );
    });
  });

  describe('getBaseCurrency', () => {
    it('should select BaseCurrency', () => {
      expect(selectors.getBaseCurrency.projector(state)).toEqual(
        state.baseCurrency
      );
    });
  });

  describe('getDate', () => {
    it('should select Date', () => {
      expect(selectors.getDate.projector(state)).toEqual(state.date);
    });
  });

  describe('getRatesKey', () => {
    it('should select RatesKey', () => {
      expect(selectors.getRatesKey.projector(state)).toEqual(state.ratesKey);
    });
  });
});
