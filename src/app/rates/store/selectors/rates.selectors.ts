import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ratesFeatureKey, RatesState } from '../reducers/rates.reducer';

export const getRatesFeatureState = createFeatureSelector<RatesState>(
  ratesFeatureKey
);

export const getRates = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.rates
);

export const getHistoricRates = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.historicRates
);

export const getRatesLoadingStatus = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.ratesLoadingStatus
);

export const getHistoricRatesLoadingStatus = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.historicRatesLoadingStatus
);

export const getBaseCurrency = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.baseCurrency
);

export const getDate = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.date
);

export const getRatesKey = createSelector(
  getRatesFeatureState,
  (state: RatesState) => state.ratesKey
);
