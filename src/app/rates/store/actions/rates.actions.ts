import { createAction, props } from '@ngrx/store';
import { Currencies } from '../../models/currencies';
import { RatesApiResponse } from '../../models/rates-api-response';
import { HistoricRatesApiResponse } from '../../models/historic-rates-api-response';

export const loadRates = createAction(
  '[Rates] Load Rates',
  props<{ baseCurrency: Currencies; date?: string }>()
);

export const loadRatesSuccess = createAction(
  '[Rates] Load Rates Success',
  props<{ ratesApiResponse: RatesApiResponse; ratesKey: string }>()
);

export const loadRatesFailure = createAction('[Rates] Load Rates Failure');

export const loadHistoricRates = createAction(
  '[Rates] Load Historic Rates',
  props<{
    baseCurrency: Currencies;
    start_at: string;
    end_at: string;
    currency: Currencies;
  }>()
);

export const loadHistoricRatesSuccess = createAction(
  '[Rates] Load Historic Rates Success',
  props<{ historicRates: HistoricRatesApiResponse; currency: Currencies }>()
);

export const loadHistoricRatesFailure = createAction(
  '[Rates] Load Historic Rates Failure'
);

export const changeBaseCurrency = createAction(
  '[Rates] Change Base Currency',
  props<{ currency: Currencies }>()
);

export const changeSelectedDate = createAction(
  '[Rates] Change Selected Date',
  props<{ date: string }>()
);
