import { Currencies } from '../../models/currencies';
import { TestBed } from '@angular/core/testing';
import { RatesEffects } from './rates.effects';
import { RatesApiService } from '../../services/rates-api.service';
import { hot, ObservableWithSubscriptions } from 'jest-marbles';
import * as RatesActions from '../actions/rates.actions';
import { of, throwError } from 'rxjs';
import { RatesApiResponse } from '../../models/rates-api-response';
import { Rates } from '../../models/rates';
import { provideMockActions } from '@ngrx/effects/testing';
import { HistoricRatesApiResponse } from '../../models/historic-rates-api-response';

describe('RatesEffects', () => {
  let actions: ObservableWithSubscriptions;
  let effects: RatesEffects;
  let ratesApiService: RatesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RatesEffects,
        {
          provide: RatesApiService,
          useValue: { getAllRates: () => {}, getRateHistory: () => {} }
        },
        provideMockActions(() => actions)
      ]
    });
    effects = TestBed.inject(RatesEffects);
    ratesApiService = TestBed.inject(RatesApiService);
  });

  describe('loadRates$', () => {
    it('should fire success action if getAllRates responds', () => {
      const ratesApiResponse: RatesApiResponse = {
        date: '2019-01-01',
        base: Currencies.GBP,
        rates: {} as Rates
      };
      spyOn(ratesApiService, 'getAllRates').and.returnValue(
        of(ratesApiResponse)
      );

      actions = hot('-a-', {
        a: RatesActions.loadRates({ baseCurrency: Currencies.GBP })
      });

      const expected = hot('-a-', {
        a: RatesActions.loadRatesSuccess({ ratesApiResponse, ratesKey: 'GBP_' })
      });
      expect(effects.loadRates$).toBeObservable(expected);
    });

    it('should fire failure action if getAllRates fails', () => {
      spyOn(ratesApiService, 'getAllRates').and.returnValue(
        throwError('error')
      );

      actions = hot('-a-', {
        a: RatesActions.loadRates({ baseCurrency: Currencies.GBP })
      });

      const expected = hot('-a-', {
        a: RatesActions.loadRatesFailure()
      });
      expect(effects.loadRates$).toBeObservable(expected);
    });
  });

  describe('loadHistoricRates$', () => {
    it('should fire success action if getRateHistory responds', () => {
      const historicRatesApiResponse: HistoricRatesApiResponse = {
        start_at: '2019-01-01',
        end_at: '2019-01-09',
        base: Currencies.GBP,
        rates: { date: {} as Rates }
      };
      spyOn(ratesApiService, 'getRateHistory').and.returnValue(
        of(historicRatesApiResponse)
      );

      actions = hot('-a-', {
        a: RatesActions.loadHistoricRates({
          baseCurrency: Currencies.GBP,
          start_at: '2019-01-01',
          end_at: '2019-01-09',
          currency: Currencies.EUR
        })
      });

      const expected = hot('-a-', {
        a: RatesActions.loadHistoricRatesSuccess({
          historicRates: historicRatesApiResponse,
          currency: Currencies.EUR
        })
      });
      expect(effects.loadHistoricRates$).toBeObservable(expected);
    });

    it('should fire failure action if getRateHistory fails', () => {
      spyOn(ratesApiService, 'getRateHistory').and.returnValue(
        throwError('error')
      );

      actions = hot('-a-', {
        a: RatesActions.loadHistoricRates({
          baseCurrency: Currencies.GBP,
          start_at: '2019-01-01',
          end_at: '2019-01-09',
          currency: Currencies.EUR
        })
      });

      const expected = hot('-a-', {
        a: RatesActions.loadHistoricRatesFailure()
      });
      expect(effects.loadHistoricRates$).toBeObservable(expected);
    });
  });
});
