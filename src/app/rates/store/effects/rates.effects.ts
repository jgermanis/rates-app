import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import * as RatesActions from '../actions/rates.actions';
import { RatesApiService } from '../../services/rates-api.service';

@Injectable()
export class RatesEffects {
  loadRates$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RatesActions.loadRates),
      mergeMap(({ baseCurrency, date }) => {
        const key = `${baseCurrency}_${date ? date : ''}`;
        return this.ratesApiService.getAllRates(baseCurrency, date).pipe(
          map(rates =>
            RatesActions.loadRatesSuccess({
              ratesApiResponse: rates,
              ratesKey: key
            })
          ),
          catchError(() => of(RatesActions.loadRatesFailure()))
        );
      })
    )
  );

  loadHistoricRates$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RatesActions.loadHistoricRates),
      mergeMap(({ baseCurrency, start_at, end_at, currency }) => {
        return this.ratesApiService
          .getRateHistory(baseCurrency, start_at, end_at, currency)
          .pipe(
            map(rates =>
              RatesActions.loadHistoricRatesSuccess({
                historicRates: rates,
                currency
              })
            ),
            catchError(() => of(RatesActions.loadHistoricRatesFailure()))
          );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private ratesApiService: RatesApiService
  ) {}
}
